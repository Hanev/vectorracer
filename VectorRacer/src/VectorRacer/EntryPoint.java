package VectorRacer;

import java.io.FileNotFoundException;
import java.io.IOException;

import VectorRacer.VisualizationEngine.VectorRacerFrame;

public class EntryPoint {

	public static void main(String[] args) throws IOException, InterruptedException {
		// Load times:
		// Map1 -> Fast
		// Map2 -> Few seconds
		// Map3 -> About 20 seconds
		// Map4 -> Few seconds
		// Map5 -> About a minute
		// Map6 -> Fast
		// Map7 -> Fast
		
		VectorRacerMap map = GetMap7();
		
		VectorRacerFrame frame = new VectorRacerFrame(map);
		frame.CreateGui();
	}
	
	private static VectorRacerMap GetMap1() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map1.txt")
				.SetStartingPosition(10, 23)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {'a'})
				.Build();
	}
	
	private static VectorRacerMap GetMap2() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map2.txt")
				.SetStartingPosition(7, 3)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
	
	private static VectorRacerMap GetMap3() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map3.txt")
				.SetStartingPosition(6, 3)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
	
	private static VectorRacerMap GetMap4() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map4.txt")
				.SetStartingPosition(7, 15)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
	
	private static VectorRacerMap GetMap5() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map5.txt")
				.SetStartingPosition(1, 1)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
	
	private static VectorRacerMap GetMap6() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map6.txt")
				.SetStartingPosition(7, 19)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
	
	private static VectorRacerMap GetMap7() throws FileNotFoundException, IOException {
		return VectorRacerMap
				.NewBuilder()
				.SetMap("src/SampleMaps/map7.txt")
				.SetStartingPosition(20, 17)
				.SetFinishSign('f')
				.SetPassableSign('1')
				.SetNotPassableSign('0')
				.SetCheckpoints(new char[] {})
				.Build();
	}
}