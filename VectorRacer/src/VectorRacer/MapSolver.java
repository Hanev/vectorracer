package VectorRacer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class MapSolver {
	private VectorRacerMap vectorRacerMap;
	private PositionContext startingPosition;
	private int currentShortestPathLength;
	
	private char finishSign;
	private char passableSign;
	private char notPassableSign;
	private char[] checkPointSigns;
	private char[][] map;
	
	private Stack<PositionContext> currentPath;
	private Set<String> currentPathProcessedPositions;
	private Set<String> processedPositions;
	private Map<String, Integer> processedPositionPreviousPaths;
	
	private Map<String, Stack<PositionContext>> finishedPathsFromPositionCache;
	private Map<Integer, ArrayList<Stack<PositionContext>>> pathsGroupedByLength;
	
	public MapSolver(VectorRacerMap vectorRacerMap) {
		this.vectorRacerMap = vectorRacerMap;
		this.map = vectorRacerMap.GetMap();
		this.finishSign = vectorRacerMap.GetFinishSign();
		this.passableSign = vectorRacerMap.GetPassableSign();
		this.notPassableSign = vectorRacerMap.GetNotPassableSign();
		this.checkPointSigns = vectorRacerMap.GetCheckpoints();
		this.startingPosition = vectorRacerMap.GetStartingPosition();
		this.currentShortestPathLength = this.map.length * this.map[0].length;
		
		this.currentPath = new Stack<PositionContext>();
		this.currentPathProcessedPositions = new HashSet<String>();
		this.processedPositions = new HashSet<String>();
		this.processedPositionPreviousPaths = new HashMap<String, Integer>();
		
		this.finishedPathsFromPositionCache = new HashMap<String, Stack<PositionContext>>();
		this.pathsGroupedByLength = new HashMap<Integer, ArrayList<Stack<PositionContext>>>();
	}
	
	public Stack<PositionContext> GetShortestPath() {
		FindPaths(this.startingPosition);
		int shortestPathSize = Collections.min(pathsGroupedByLength.keySet());
		
		return pathsGroupedByLength.get(shortestPathSize).get(0);
	}
	
	private void FindPaths(PositionContext curPosition) {		
		int curPositionRow = curPosition.GetRow();
		int curPositionCol = curPosition.GetCol();
		int curPositionVelocityRow = curPosition.GetVelocityRow();
		int curPositionVelocityCol = curPosition.GetVelocityCol();
		
		Boolean rowExists = curPositionRow >= 0 && curPositionRow < map.length;
		Boolean colExists = rowExists && curPositionCol >= 0 && curPositionCol < map[curPositionRow].length;
		
		if(!rowExists || !colExists) {			
			return;
		}
		
		if(currentShortestPathLength < currentPath.size()) {
			return;
		}
		
		char curPositionChar = map[curPositionRow][curPositionCol];
		String curPositionKey = curPosition.GenerateKey();
		
		if(this.currentPathProcessedPositions.contains(curPositionKey) || curPositionChar == this.notPassableSign) {
			return;
		}
				
		if (finishedPathsFromPositionCache.containsKey(curPositionKey)) {
			Stack<PositionContext> curPathStack = new Stack<PositionContext>();
			
			curPathStack.addAll(currentPath);
			curPathStack.addAll(finishedPathsFromPositionCache.get(curPositionKey));

			ChangeShorterPathIfNeeded(curPathStack);
			return;
		}
		
		if (processedPositions.contains(curPositionKey) && processedPositionPreviousPaths.get(curPositionKey) <= currentPath.size()) {
			return;
		}
		
		Set<Character> charactersPassedThrough = vectorRacerMap.CharsPassedThrough(curPosition);
		
		for (Character character : charactersPassedThrough) {
			if (character == this.notPassableSign) {
				return;
			}
			
			if (IsCharCheckpointSign(character)) {
				curPosition.GetCheckpointsPassed().add(character);
			}
			
			if (character == this.finishSign && this.checkPointSigns.length == curPosition.GetCheckpointsPassed().size()) {
				currentPath.push(curPosition);
				
				ChangeShorterPathIfNeeded(currentPath);
				CacheCurrentPath();
				
				currentPath.pop();
				return;
			}
		}
		
		if(curPositionChar == this.passableSign) {
			processedPositionPreviousPaths.put(curPositionKey, currentPath.size());
			
			currentPath.push(curPosition);
			currentPathProcessedPositions.add(curPositionKey);
			processedPositions.add(curPositionKey);
						
			int newCenterRow = curPositionRow + curPositionVelocityRow;
			int newCenterCol = curPositionCol + curPositionVelocityCol;
						
			for (int velocityRowOffset = -1; velocityRowOffset <= 1; velocityRowOffset++) {
				for (int velocityColOffset = -1; velocityColOffset <= 1; velocityColOffset++) {
					int newPositionRow = newCenterRow + velocityRowOffset;
					int newPositionCol = newCenterCol + velocityColOffset;
					int newVelocityRow = curPositionVelocityRow + velocityRowOffset;
					int newVelocityCol = curPositionVelocityCol + velocityColOffset;
					
					Set<Character> newPositionCheckpoints = new HashSet<Character>();
					newPositionCheckpoints.addAll(curPosition.GetCheckpointsPassed());
					
					PositionContext newPosition = new PositionContext(newPositionRow, newPositionCol, newVelocityRow, newVelocityCol, newPositionCheckpoints);
					FindPaths(newPosition);
				}
			}
			
			currentPathProcessedPositions.remove(curPosition.GenerateKey());
			currentPath.pop();
		}
	}
	
	private void CacheCurrentPath() {
		for (int i = 0; i < currentPath.size(); i++) {
			PositionContext curPosition = currentPath.get(i);
			String curPositionKey = curPosition.GenerateKey();

			Stack<PositionContext> curPositionPath = new Stack<PositionContext>();
			
			for (int j = i; j < currentPath.size(); j++) {
				curPositionPath.push(currentPath.get(j));
			}
			
			Boolean finishedPathIsCached = finishedPathsFromPositionCache.containsKey(curPositionKey);
			Boolean finishedPathIsCachedButLonger = finishedPathIsCached && finishedPathsFromPositionCache.get(curPositionKey).size() > curPositionPath.size();
			
			if(!finishedPathIsCached || finishedPathIsCachedButLonger) {
				finishedPathsFromPositionCache.put(curPositionKey, curPositionPath);
			}
		}
	}
	
	private void ChangeShorterPathIfNeeded(Stack<PositionContext> newPath) {
		Stack<PositionContext> newStack = new Stack<PositionContext>();
		newStack.addAll(newPath);
		
		if (!pathsGroupedByLength.containsKey(newStack.size())) {
			pathsGroupedByLength.put(newStack.size(), new ArrayList<Stack<PositionContext>>());
		}
		
		pathsGroupedByLength.get(newStack.size()).add(newStack);
		
		if (newStack.size() < currentShortestPathLength) {
			currentShortestPathLength = newStack.size();
		}
	}
	
	private Boolean IsCharCheckpointSign(char character) {
		for (char c : checkPointSigns) {
			if(c == character) {
				return true;
			}
		}
		
		return false;
	}
}