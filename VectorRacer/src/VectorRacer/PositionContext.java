package VectorRacer;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class PositionContext {
	private int positionRow;
	private int positionCol;
	private int velocityRow;
	private int velocityCol;
	private Set<Character> checkpointsPassed;
	
	public PositionContext(int posRow, int posCol, int velocityRow, int velocityCol) {
		this.positionRow = posRow;
		this.positionCol = posCol;
		this.velocityRow = velocityRow;
		this.velocityCol = velocityCol;
		this.checkpointsPassed = new HashSet<Character>();
	}
	
	public PositionContext(int posRow, int posCol, int velocityRow, int velocityCol, Set<Character> checkpointsPassed) {
		this(posRow, posCol, velocityRow, velocityCol);
		this.checkpointsPassed = checkpointsPassed;
	}
	
	public int GetRow() {
		return positionRow;
	}
	
	public int GetCol() {
		return positionCol;
	}
	
	public int GetVelocityRow() {
		return this.velocityRow;
	}
	
	public int GetVelocityCol() {
		return this.velocityCol;
	}
	
	public Set<Character> GetCheckpointsPassed() {
		return this.checkpointsPassed;
	}
	
	public String GenerateKey() {
		StringBuilder keyBuilder = new StringBuilder();
		
		keyBuilder.append("row:").append(this.positionRow)
				  .append(" col:").append(this.positionCol)
				  .append(" velRow:").append(this.velocityRow)
				  .append(" velCol").append(this.velocityCol)
				  .append(" cp:");
		
		for (Character character : checkpointsPassed) {
			keyBuilder.append(character);
			keyBuilder.append(", ");
		}
		
		return keyBuilder.toString();
	}
	
	public void setCheckpointsPassed(Set<Character> checkpoints) {
		this.checkpointsPassed = checkpoints;
	}
	
	public String toString() {
		return this.positionRow +", " + this.positionCol;
	}
}