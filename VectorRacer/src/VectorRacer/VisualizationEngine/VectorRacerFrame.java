package VectorRacer.VisualizationEngine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import VectorRacer.VectorRacerMap;

public class VectorRacerFrame {
	private VectorRacerMap vectorRacerMap;
	
	public VectorRacerFrame(VectorRacerMap vectorRacerMap) {
		this.vectorRacerMap = vectorRacerMap;
	}
	
	public void CreateGui() {
		JFrame frame = new JFrame();
		
        MapComponent mapComponent = new MapComponent(this.vectorRacerMap);
        JButton showPathButton = new JButton("Show shortest path");
        
        showPathButton.setBounds(700, 660, 200, 30);
        
        showPathButton.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent e) {
				mapComponent.ShowPath();
			}
        });
       
        frame.add(showPathButton);
		frame.add(mapComponent);
		
		frame.setSize(900, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}