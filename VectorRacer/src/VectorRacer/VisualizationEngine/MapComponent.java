package VectorRacer.VisualizationEngine;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.swing.JComponent;

import VectorRacer.MapSolver;
import VectorRacer.PositionContext;
import VectorRacer.VectorRacerMap;

public class MapComponent extends JComponent {
	private static final int cellSize = 15;
	
	private char[][] map;
	private Boolean shouldShowPath;
	private VectorRacerMap vectorRacerMap;
	
	private Map<Character, Color> signColors;
	private Stack<PositionContext> shortestPath;

	public MapComponent(VectorRacerMap vectorRacerMap) {
		this.shouldShowPath = false;
		this.vectorRacerMap = vectorRacerMap;
		this.map = vectorRacerMap.GetMap();
		
		this.signColors = new HashMap<Character, Color>();
		this.signColors.put(vectorRacerMap.GetPassableSign(), Color.white);
		this.signColors.put(vectorRacerMap.GetNotPassableSign(), Color.gray);
		this.signColors.put(vectorRacerMap.GetFinishSign(), Color.black);
		
		for (char checkpointSign : vectorRacerMap.GetCheckpoints()) {
			this.signColors.put(checkpointSign, Color.orange);
		}
		
		MapSolver solver = new MapSolver(this.vectorRacerMap);
		this.shortestPath = solver.GetShortestPath();
	}
	
	protected void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);

		FillCellsByType(graphics);
		AddBordersToCells(graphics);
		FillStartingPosition(graphics);
		if (this.shouldShowPath) {
			ShowShortestPath(graphics);			
		}
	}
	
	private void FillCellsByType(Graphics graphics) {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				char cell = map[row][col];
				Color cellColor = this.signColors.get(cell);
				
				graphics.setColor(cellColor);
				graphics.fillRect(col * cellSize, row * cellSize, cellSize, cellSize);
			}
		}
	}
	
	private void AddBordersToCells(Graphics graphics) {
		for (int row = 0; row < map.length; row++) {
			for (int col = 0; col < map[row].length; col++) {
				graphics.setColor(Color.cyan);
				graphics.drawRect(col * cellSize, row * cellSize, cellSize, cellSize);
			}
		}
	}
	
	private void FillStartingPosition(Graphics graphics) {
		int startRow = this.vectorRacerMap.GetStartingPosition().GetRow();
		int startCol = this.vectorRacerMap.GetStartingPosition().GetCol();
		
		graphics.setColor(Color.green);
		graphics.fillRect(startCol * cellSize, startRow * cellSize, cellSize, cellSize);
	}
	
	private void ShowShortestPath(Graphics graphics) {
		graphics.setColor(Color.red);
		
		for (PositionContext position : shortestPath) {
			int lineEndRow = position.GetRow() * cellSize + cellSize / 2;
			int lineEndCol = position.GetCol() * cellSize + cellSize / 2;
			
			int lineBeginRow = (position.GetRow() - position.GetVelocityRow()) * cellSize + cellSize / 2;
			int lineBeginCol = (position.GetCol() - position.GetVelocityCol()) * cellSize + cellSize / 2;
			
			graphics.drawLine(lineBeginCol , lineBeginRow, lineEndCol, lineEndRow);
			graphics.fillRect(lineEndCol - cellSize / 6, lineEndRow - cellSize / 6, cellSize / 3, cellSize / 3);
		}
	}
	
	public void ShowPath() {
		this.shouldShowPath = true;
		this.repaint();
	}
}