package VectorRacer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VectorRacerMap {	
	private PositionContext startingPosition;
	private char[][] map;
	private char finishSign;
	private char passableSign;
	private char notPassableSign;
	private char[] checkPointSigns;
	private Map<String, Set<Character>> charsPassedThroughCache;
	
	private VectorRacerMap(char[][] map, PositionContext startingPosition, char finishSign, char passable, char notPassable, char[] checkPointSigns) {
		this.map = map;
		this.startingPosition = startingPosition;
		this.finishSign = finishSign;
		this.passableSign = passable;
		this.notPassableSign = notPassable;
		this.checkPointSigns = checkPointSigns;
		
		this.charsPassedThroughCache = new HashMap<String, Set<Character>>();
	}
	
	public static EmptyVectorRaceMapBuilder NewBuilder() {
		return new VectorRacerMapBuilder().New();
	}
	
	public char[][] GetMap() {
		return this.map;
	}
	
	public char GetFinishSign() {
		return this.finishSign;
	}
	
	public char GetPassableSign() {
		return this.passableSign;
	}
	
	public char GetNotPassableSign() {
		return this.notPassableSign;
	}
	
	public char[] GetCheckpoints() {
		return this.checkPointSigns;
	}
	
	public PositionContext GetStartingPosition() {
		return this.startingPosition;
	}
	
	public Set<Character> CharsPassedThrough(PositionContext position){
		String curPositionKey = position.GenerateKey();
		
		if (charsPassedThroughCache.containsKey(curPositionKey)) {
			return charsPassedThroughCache.get(curPositionKey);
		}
		
		Set<Character> charactersPassed = new HashSet<Character>();
		
		int startingPositionRow = Math.abs(position.GetRow() - position.GetVelocityRow());
		int startingPositionCol = Math.abs(position.GetCol() - position.GetVelocityCol());
		int endingPositionRow = position.GetRow();
		int endingPositionCol = position.GetCol();
		
		int rowDirection = 1;
		int colDirection = 1;
		
		if(startingPositionRow > endingPositionRow) {
			rowDirection = -1;
		}
		
		if(startingPositionCol > endingPositionCol) {
			colDirection = -1;
		}
		
		int width = Math.abs(startingPositionCol - endingPositionCol) + 1;
		int height = Math.abs(startingPositionRow - endingPositionRow) + 1;
		
		double rowsPassedPerCol = (double)height / width;
		
		for (int col = startingPositionCol, processedCols = 0; col != endingPositionCol + colDirection; col += colDirection, processedCols++) {
			int minRowPoint = startingPositionRow + ((int) Math.floor(processedCols * rowsPassedPerCol) * rowDirection);
			int maxRowPoint = startingPositionRow + ((int) Math.ceil(processedCols * rowsPassedPerCol + rowsPassedPerCol) * rowDirection);
			int rowsToPass = Math.abs(maxRowPoint - minRowPoint);
						
			for (int row = minRowPoint, passedRows = 0; passedRows < rowsToPass; row += rowDirection, passedRows++) {
				charactersPassed.add(map[row][col]);
			}
		}
		
		charsPassedThroughCache.put(curPositionKey, charactersPassed);
		
		return charactersPassed;
	}
	
	public void PrintMap() {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				System.out.print(map[i][j]);
			}
			
			System.out.println();
		}
	}
	
	public static class VectorRacerMapBuilder 
		implements EmptyVectorRaceMapBuilder, WithMap, WithFinishSign, WithPassableSign, 
				   WithNotPassableSign, WithCheckpointSigns, WithStartingPosition {
		
		private int fixedSigns = 3;
		private PositionContext startingPosition;
		private char[][] map;
		private char finishSign;
		private char passable;
		private char notPassable;
		private char[] checkPoints;
		
		private int numberOfDifferentSignsRegistered = 0;
		private Set<Character> charactersRegistered = new HashSet<Character>();
		
		public EmptyVectorRaceMapBuilder New() {
			return new VectorRacerMapBuilder();
		}

		public WithMap SetMap(String filePath) throws IOException {
			ArrayList<ArrayList<Character>> map = new ArrayList<ArrayList<Character>>();  
            
			BufferedReader reader = new BufferedReader(new FileReader(filePath));         
			                                                                              
			try {                                                                         
			    String line = reader.readLine();                                          
			                                                                              
			    while (line != null) {                                                    
			    	char[] lineCharacters = line.toCharArray();                           
			    	ArrayList<Character> chars = new ArrayList<Character>();              
				                                                                          
				    for (int i = 0; i < lineCharacters.length; i++) {                     
						chars.add(i, lineCharacters[i]);                                  
				    }                                                                     
				                                                                          
				    map.add(chars);                                                       
				                                                                          
			        line = reader.readLine();                                             
			    }                                                                         
			} finally {                                                                   
			    reader.close();                                                           
			}                                                                             
			                                                                              
			int rows = map.size();                                                        
			int cols = map.get(0).size();                                                 
			                                                                              
			char[][] charMap = new char[rows][cols];                                      
			                                                                              
			for (int i = 0; i < rows; i++) {                                              
				for (int j = 0; j < cols; j++) {                                          
					charMap[i][j] = map.get(i).get(j);                                    
				}                                                                         
			}                                                                             

			this.map = charMap;
			return this;
		}
		
		public WithStartingPosition SetStartingPosition(int row, int col) {
			if(!IsValidPosition(row, col)) {
				throw new ExceptionInInitializerError("Starting position should be a point from the map.");
			}
			
			this.startingPosition = new PositionContext(row, col, 0, 0);			
			return this;
		}

		public VectorRacerMap Build() {
			if (charactersRegistered.size() != numberOfDifferentSignsRegistered) {
				throw new ExceptionInInitializerError("Duplicate signs are not allowed!");
			}
			
			return new VectorRacerMap(map, this.startingPosition, finishSign, passable, notPassable, checkPoints);
		}

		public WithCheckpointSigns SetCheckpoints(char[] signs) {
			this.checkPoints = signs;
			
			for (char c : signs) {
				this.charactersRegistered.add(c);
				this.numberOfDifferentSignsRegistered++;
			}
			
			return this;
		}

		public WithNotPassableSign SetNotPassableSign(char sign) {
			this.notPassable = sign;
			this.charactersRegistered.add(sign);
			this.numberOfDifferentSignsRegistered++;
			
			return this;
		}

		public WithPassableSign SetPassableSign(char sign) {
			this.passable = sign;
			this.charactersRegistered.add(sign);
			this.numberOfDifferentSignsRegistered++;
			
			return this;
		}

		public WithFinishSign SetFinishSign(char sign) {
			this.finishSign = sign;
			this.charactersRegistered.add(sign);
			this.numberOfDifferentSignsRegistered++;
			
			return this;
		}
		
		private Boolean IsValidPosition(int row, int col) {
			Boolean rowExists = row >= 0 && row < map.length;
			Boolean colExists = rowExists && col >= 0 && col < map[row].length;
			
			if(!rowExists || !colExists) {			
				return false;
			}
			
			return true;
		}
	}
	
	public interface EmptyVectorRaceMapBuilder {
		WithMap SetMap(String filePath) throws FileNotFoundException, IOException;
	}
	
	public interface WithMap {
		WithStartingPosition SetStartingPosition(int row, int col);
	}
	
	public interface WithStartingPosition {
		WithFinishSign SetFinishSign(char sign);
	}
	
	public interface WithFinishSign {
		WithPassableSign SetPassableSign(char sign);
	}
	
	public interface WithPassableSign {
		WithNotPassableSign SetNotPassableSign(char sign);
	}
	
	public interface WithNotPassableSign {
		WithCheckpointSigns SetCheckpoints(char[] signs);
	}
	
	public interface WithCheckpointSigns {
		VectorRacerMap Build();
	}
}